package org.tom;


import com.sun.tools.attach.AttachNotSupportedException;
import com.sun.tools.attach.VirtualMachine;
import com.sun.tools.attach.VirtualMachineDescriptor;

import javax.management.*;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class Client {
    public static void main(String args[]) {

        VirtualMachine vm = null;
        List<VirtualMachineDescriptor> dsc = VirtualMachine.list();
        for (VirtualMachineDescriptor d: dsc) {
            if (d.displayName().contains("org.tom.t1")) {
                try {
                    vm = VirtualMachine.attach(d);
                } catch (AttachNotSupportedException | IOException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            Properties p = vm.getAgentProperties();
            final JMXServiceURL target = new JMXServiceURL(vm.startLocalManagementAgent());
            final JMXConnector con = JMXConnectorFactory.connect(target);
            final MBeanServerConnection remote = con.getMBeanServerConnection();

            final ObjectName bean = new ObjectName("MY:name=CounterMBean");
            Long v = (Long) remote.getAttribute(bean, "Value");
            Object o = remote.getAttribute(bean, "Table");
            System.out.println(v);


            System.out.println("aa");
        } catch (IOException | MalformedObjectNameException | InstanceNotFoundException | AttributeNotFoundException | MBeanException | ReflectionException e) {
            e.printStackTrace();
        }
    }
}
