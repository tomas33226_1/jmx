package org.tom;


import java.util.List;

public interface CounterMBean {
    long get();
    long getValue();
    List<Long> getTable();
}
