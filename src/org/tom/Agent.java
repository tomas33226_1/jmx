package org.tom;


import javax.management.*;
import java.lang.management.ManagementFactory;

public class Agent {
    private MBeanServer mbs = null;
    public Agent() {
        mbs = ManagementFactory.getPlatformMBeanServer();
        Counter c = new Counter();
        ObjectName counterName = null;

        try {
            counterName = new ObjectName("MY:name=CounterMBean");
            mbs.registerMBean(c, counterName);
        } catch (MalformedObjectNameException | NotCompliantMBeanException | MBeanRegistrationException | InstanceAlreadyExistsException e) {
            e.printStackTrace();
        }
    }
}
