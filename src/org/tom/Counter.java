package org.tom;


import java.util.ArrayList;
import java.util.List;

public class Counter implements CounterMBean {
    @Override
    public long get() {
        return 100;
    }
    public long getValue() {
        return 200;
    }

    @Override
    public List<Long> getTable() {
        List<Long> ret = new ArrayList<>();
        ret.add((long) 10);
        ret.add((long) 20);
        return ret;
    }
}
